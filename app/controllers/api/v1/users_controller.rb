# frozen_string_literal: true

# API module
module Api
  # V1 Module
  module V1
    # Current Users controller class
    class UsersController < ApplicationController
      before_action :authenticate_user!

      def index
        render json: current_user, status: :ok
      end
    end
  end
end
