# Movnychok API

> Style checker and lint tool for this app.

[![pipeline status](https://gitlab.com/movnychok_group/movnychok_api/badges/main/pipeline.svg)](https://gitlab.com/movnychok_group/movnychok_api/-/commits/main)
[![coverage report](https://gitlab.com/movnychok_group/movnychok_api/badges/main/coverage.svg)](https://gitlab.com/movnychok_group/movnychok_api/-/commits/main)

This is study application to learn something new in Rails. Add and setup Docker for this project. Also add GitLab CI/CD configurations. Create React.JS application and connect with Rails API part.

## Usage

* Ruby v.3.2.3 with [asdf](https://mac.install.guide/rubyonrails/7).
* [Rails 7.0.8](https://rubyonrails.org/2023/9/9/Rails-7-0-8-has-been-released).
* PostgreSQL 14.10.

### Local run Recommendations

1. Clone the new repository to your local machine with SSH:

   ```shell
     git@gitlab.com:movnychok_group/movnychok_api.git
   ```

   or with HTTPS

   ```shell
     https://gitlab.com/movnychok_group/movnychok_api.git
   ```

1. Run `bundle install`.
1. Run `rails db:create db:migrate db:seed`.
1. Run `rails s` to start your api server on port 3001.
1. Open in your browser [localhost:3001](http://localhost:3001/).
1. Run spec tests `rspec spec`.
1. Run rubocop linter `rubocop` and `rubocop -a` or `rubocop -A` to autocorrect.
1. Run swagger documentation based on request tests `rails rswag`.

### Docker run Recommendations

1. To build docker immage run `docker-compose build`.
1. To prepare db in docker run `docker-compose run web rails db:create db:migrate db:seed`.
1. To run your server in docker run `docker-compose up`.
1. To run specs in docker use `docker-compose run web rspec`.
1. To run rubocop in docker use `docker-compose run web rubocop`.
1. To run swagger in docker use `docker-compose run web rails rswag`.

## Contributing

1. Fork it!
2. Create your feature branch `git checkout -b my-new-feature`
3. Commit your changes `git commit -am 'Add some feature'`
4. Push to the branch `git push origin my-new-feature`
5. Submit a pull request 😄
