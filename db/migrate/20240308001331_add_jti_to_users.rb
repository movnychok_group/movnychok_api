# frozen_string_literal: true

# User's JTI migration
class AddJtiToUsers < ActiveRecord::Migration[7.1]
  disable_ddl_transaction!

  def change
    add_column :users, :jti, :string, null: false, default: ''
    add_index :users, :jti, unique: true, algorithm: :concurrently
  end
end
