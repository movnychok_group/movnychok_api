require 'rails_helper'

RSpec.describe UserSerializer do
  let(:user)          { FactoryBot.create(:user) }
  let(:serializer)    { described_class.new(user) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer).as_json }

  it 'includes the expected attributes' do
    expect(serialization.keys).to contain_exactly(:id, :email, :name, :created_at, :updated_at, :posts)
  end
end
