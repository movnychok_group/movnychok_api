require 'swagger_helper'

RSpec.describe 'Api::V1::UsersController', type: :request do
  let(:user) { FactoryBot.create(:user) }

  path '/api/v1/users' do
    get('Returns authorized current user') do
      tags 'Users'
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      response(200, 'Successful') do
        let(:Authorization) { auth_token(user) }

        it 'returns current_user' do
          data = response.parsed_body

          expect(data['id']).to eq(user.id)
          expect(response).to have_http_status(:success)
        end

        run_test!
      end

      response(401, 'Unauthorized') do
        let(:Authorization) { nil }

        it 'doesn`t returns current_user' do
          expect(response).to have_http_status(:unauthorized)
        end

        run_test!
      end
    end
  end
end
