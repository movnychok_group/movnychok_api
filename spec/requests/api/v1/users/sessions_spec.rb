require 'rails_helper'

RSpec.describe 'Api::V1::Users::SessionsController', type: :request do # rubocop:disable Metrics/BlockLength
  let(:user) { FactoryBot.create(:user) }

  # Login
  path '/login' do # rubocop:disable Metrics/BlockLength
    post 'Login user' do # rubocop:disable Metrics/BlockLength
      tags 'Users'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            type: :object,
            properties: {
              email: { type: :string },
              password: { type: :string }
            },
            required: %w[email password]
          }
        },
        required: [:user]
      }

      response '200', 'Successful (Logged in successfully)' do
        let(:email)    { user.email }
        let(:password) { user.password }

        it 'returns data loged in user' do
          post '/login', params: { user: { email:, password: } }

          data = response.parsed_body

          expect(data.dig('data', 'email')).to eq(user.email)
          expect(response).to have_http_status(:ok)
        end
      end

      response '401', 'Unauthorized (Invalid Email or password.)' do
        it 'returns invalid email or password' do
          post '/login', params: { user: { email: user.email, password: '' } }

          expect(response).to have_http_status(:unauthorized)
          expect(response.body).to include('Invalid Email or password.')
        end
      end
    end
  end

  # Logout
  path '/logout' do
    delete('Logout user') do
      tags 'Users'
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      response(200, 'Successful (Logged out successfully)') do
        let(:Authorization) { auth_token(user) }

        it 'logs out the user' do
          delete '/logout', headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:ok)
          expect(response.body).to include('logged out successfully')
        end
      end

      response(401, "Unauthorized (Couldn't find an active session)") do
        it 'returns unauthorized for missing or invalid token' do
          delete '/logout'

          expect(response).to have_http_status(:unauthorized)
          expect(response.body).to include("Couldn't find an active session.")
        end
      end
    end
  end
end
