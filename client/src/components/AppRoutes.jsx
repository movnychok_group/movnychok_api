import { Route, Routes } from 'react-router-dom';
import PostsList from '../features/posts/PostsList';
import PostDeatails from '../features/posts/PostDeatails';
import NewPostForm from '../features/posts/NewPostForm';

function AppRoutes() {
  return (
    <Routes>
      <Route path="/api/v1/posts/" element={<PostsList />} />
      <Route path="/api/v1/posts/:id" element={<PostDeatails />} />
      <Route path="/api/v1/posts/new" element={<NewPostForm />} />
    </Routes>
  );
}

export default AppRoutes;
