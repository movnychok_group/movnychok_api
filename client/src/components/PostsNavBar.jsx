import { Link } from 'react-router-dom';

function PostsNavBar() {
  return (
    <nav className="posts-navbar">
      <Link to="api/v1/posts/" className="lists-link">
        Posts List
      </Link>
      {' | '}
      <Link to="api/v1/posts/new" className="new-post-link">
        New Post
      </Link>
    </nav>
  );
}

export default PostsNavBar;
