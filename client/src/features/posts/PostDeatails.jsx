import { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';
import { POSTS_URL } from '../../constants';
import Spinner from '../../utils/Spinner';

function PostDeatails() {
  const [post, setPost] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    const fetchCurrentPost = async () => {
      try {
        const response = await fetch(`${POSTS_URL}/${id}`);
        if (response.ok) {
          const json = await response.json();
          setPost(json);
        } else {
          throw response;
        }
      } catch (e) {
        console.log('An error occurred:', e);
      }
    };
    fetchCurrentPost();
  }, [id]);

  if (!post) return <Spinner />;

  return (
    <div className="posts-detail">
      <h2 className="title">{post.title}</h2>
      <p className="body">{post.body}</p>
      <Link to="/api/v1/posts/" className="back-link">
        <svg
          className="back-arrow"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor">
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M6.75 15.75L3 12m0 0l3.75-3.75M3 12h18"
          />
        </svg>
        <p>Go Back</p>
      </Link>
    </div>
  );
}

export default PostDeatails;
