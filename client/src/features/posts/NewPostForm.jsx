import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { POSTS_URL } from '../../constants';

function NewPostForm() {
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    const postData = { title, body };

    const responce = await fetch(POSTS_URL, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Put here your token"
      },
      body: JSON.stringify(postData)
    });

    if (responce.ok) {
      const { id } = await responce.json();
      navigate(`/api/v1/posts/${id}`);
    } else {
      console.log('An error occurred.');
    }
  };

  return (
    <div className="create-post">
      <h2 className="page-header">Create a New Post</h2>
      <form onSubmit={handleSubmit} className="new-post-form">
        <div className="title">
          <label htmlFor="titleInput" className="label">Title:</label>
          <input
            id="titleInput"
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            className="input"
            required
          />
        </div>
        <div className="body">
          <label htmlFor="bodyInput" className="label">Body:</label>
          <textarea
            id="bodyInput"
            type="text"
            value={body}
            onChange={(e) => setBody(e.target.value)}
            placeholder="Put here text for your post..."
            className="input"
            rows="4"
            required
          />
        </div>
        <div className="posts-submission">
          <button type="submit" className="submit-btn">Create Post</button>
        </div>
      </form>
    </div>
  );
}

export default NewPostForm;
