import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { POSTS_URL } from '../../constants'; // POSTS_URL comes from the .env.development file
import { formatDate } from '../../utils/DateUtils';

function PostsList() {
  const [posts, setPosts] = useState([]);
  const [, setLoading] = useState(true);
  const [, setError] = useState(null);

  // Fetch posts from Movnychok API
  useEffect(() => {
    async function loadPosts() {
      try {
        const response = await fetch(POSTS_URL);
        if (response.ok) {
          const json = await response.json();
          setPosts(json);
          console.log(response);
        } else {
          throw response;
        }
      } catch (e) {
        setError('An error occurred. Awkward...');
        console.log('An error occurred:', e);
      } finally {
        setLoading(false);
      }
    }
    loadPosts();
  }, []);

  return (
    <div className="posts">
      {posts.map((post) => (
        <div key={post.id} className="posts-section">
          <div className="card">
            <div className="img" title="Dictionaries"></div>
            <div className="content">
              <div className="main">
                {/* <p className='text-sm text-gray-600 flex items-center'>
                  <svg className='fill-current text-gray-500 w-3 h-3 mr-2' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
                    <path d='M4 8V6a6 6 0 1 1 12 0v2h1a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-8c0-1.1.9-2 2-2h1zm5 6.73V17h2v-2.27a2 2 0 1 0-2 0zM7 6v2h6V6a3 3 0 0 0-6 0z' />
                  </svg>
                  Members only
                </p> */}
                <div className="title">
                  <Link to={`/api/v1/posts/${post.id}`} className='link'>
                    {post.title}
                  </Link>
                </div>
                <p className="body">{post.body}</p>
              </div>
              <div className="additional">
                <img className="icon" src="/MovnychokLogo.png" alt="Avatar of Writer"></img>
                <div className="info">
                  <p className="author">{post.user.name}</p>
                  <p className="date">{formatDate(post.created_at)}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}

export default PostsList;
