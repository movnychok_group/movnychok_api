export const POSTS_URL =
  // eslint-disable-next-line no-undef
  process.env.NODE_ENV === "test"
    ? "http://mocked-api-url"
    : import.meta.env.VITE_POSTS_API_URL;
