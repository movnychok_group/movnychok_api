import { BrowserRouter as Router } from 'react-router-dom';
import './styles/tailwind.css';
import NavBar from './components/PostsNavBar';
import AppRoutes from './components/AppRoutes';

function App() {
  return (
    <Router>
      <div className="app d-flex">
        <section className="header-section">
          <div className="header">
            <div className="main-header">
              <h1>Movnychok</h1>
            </div>
          </div>
          <div className="sub-header">
            <h3>
              This is my study project to learn new in React.JS, Tailwind.css and other technologies
            </h3>
          </div>
        </section>

        <hr className="divider" />
        <NavBar />
        <AppRoutes />
      </div>
    </Router>
  );
}

export default App;
