/** @type {import('tailwindcss').Config} */
import defaultTheme from 'tailwindcss/defaultTheme';

export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx,css}",
  ],
  theme: {
    extend: {
      inset: {
        '10': '4.5rem',
      },
      margin: {
        '18': '4.5rem',
      },
      padding: {
        '15': '3.75rem',
      },
      borderWidth: {
        'px': '1px',
      },
      height: {
        '100': '25rem',
      },
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
        'proxima': ['proxima-nova', 'sans-serif'],
        'open-sans': ['open-sans', 'sans-serif'],
      },
      colors: {
        'black': '#000000',
        'jet': '#131516',
        'raven': '#373D3F',
        'asher': '#555F61',
        'stone': '#707C80',
        'sterling': '#A7B0B2',
        'heather': '#C1C7C9',
        'pearl': '#DADEDF',
        'chinese-silver': '#CCCCCC',
        'lilia': '#F2F3F4',
        'white': '#FFFFFF',
        'wet-latex-blue': '#011140',
        'new-blue': '#02178C',
        'blue-genie': '#6B63FF',
      },
      backgroundImage: {
        'app-header': "url('/src/assets/boy_on_books.svg')",
        'posts-plug': "url('/src/assets/girl_with_books.svg')",
      },
      backgroundSize: {
        'auto': 'auto',
        'cover': 'cover',
        'contain': 'contain',
        '90%': '90%',
        '75%': '75%',
        '50%': '50%',
        '40%': '40%',
        '35%': '35%',
        '30%': '30%',
        '25%': '25%',
        '16': '4rem'
      }
    },
  },
  plugins: [],
}
