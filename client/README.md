# Movnychok (React + Vite)

This is study application to learn something new in React.js(with Vite and Tailwind).

## Usage

* NPM 10.5.0 [npm](https://www.npmjs.com/package/npm/v/10.5.0)
* [React.JS 18.2.0](https://react.dev/blog/2022/03/29/react-v18)

### Local run Recommendations

1. Clone the new repository to your local machine with SSH:

   ```shell
     git@gitlab.com:movnychok_group/movnychok_api.git
   ```

   or with HTTPS

   ```shell
     https://gitlab.com/movnychok_group/movnychok_api.git
   ```

1. Run `cd client`.
1. Run `npm install`.
1. Run `npm run dev`.
1. Open in your browser [localhost:5173](http://localhost:5173/).

## Contributing

1. Fork it!
2. Create your feature branch `git checkout -b my-new-feature`
3. Commit your changes `git commit -am 'Add some feature'`
4. Push to the branch `git push origin my-new-feature`
5. Submit a pull request 😄
